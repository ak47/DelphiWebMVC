DelphiWebMVC4.0： https://gitee.com/pearroom/mvc4.0<BR>
LazarusMVC: https://gitee.com/pearroom/lazarus-mvc<BR>


# DelphiWebMVC介绍:
版本5.0<BR>
安装指南：https://my.oschina.net/delphimvc/blog/1581715<BR>
我的博客：https://my.oschina.net/delphimvc<BR>
相关视频：https://space.bilibili.com/18184783<BR>
开发手册：https://www.yuque.com/suxingying/mvc5.0/vt954m<BR>
案例赏析：http://129.211.87.47:8004/<BR>

技术QQ群: 685072623<BR>
开发工具:delphi xe10 以上,xe10.4版本除外<BR>
注意:win10系统以管理员权限运行<BR>
数据库支持MySQL,SQLite,MSSQL,Oracle<BR>

[![asciicast](http://129.211.87.47/img/play.png)](http://129.211.87.47/mp4/play.mp4)

Web可视化开发工具：https://blog.csdn.net/joman5/article/details/125129259/<BR>
[![asciicast](https://img-blog.csdnimg.cn/24e4d6eac2314c3bb92591550c53ce97.png)](https://blog.csdn.net/joman5/article/details/125129259)
# 更新记录:
    MVC5.0-220731:
		1:"apidoc":true,  //控制是否可以访问接口文档
		2:增加接口设计并导出文档模式
	MVC5.0-220730:
		1:日志模块进行优化。
	MVC5.0-220729:
		1:修复MSSQL2000，2008，2012数据库分页查询问题。
	MVC5.0-220623:
		1:增加与go服务通讯模块，集成go服务开发框架
		2:增加与Python服务通信模块,集成Python服务开发框架。
		3:控制台增加go与pyhon启动按钮，及打开对应目录功能。
		4:修复已知bug
	MVC5.0-220427:
		1:增加对 Postgresql 数据库的支持

	MVC5.0-220415版本：
		1：可以自定义视图模板 前后字符。
		2：增加redis单元和配置及案例

	2022-03-11 V5.0.5
		增加CrossSocket库引用，可实现跨平台开发，
		1.进行linux等跨平台开发
		2.打开{$APPTYPE CONSOLE},{$DEFINE CROSSSOCKET}
		3.实现exe项目与资源路经分离，资源可以不在exe同级目录下
	2022-03-01 V5.0.4
		1:MVC.DB 单元增加 IConn 接口，可在任意单元引用。
		2:使用 var conn:IConn:=IIConn;来使用数据操作模块。
		3：Service 层 由于接口模式开发有些繁琐，调整为使用record 模式，同时配合DB接口方式，使业务层模块更清爽。
	2022-02-25 V5.0.3
		1：增加SQL模板支持外部SQL文件的加载
		2：增加TVerify验证模块，支持正则表达式的验证。
		3：修复已知bug
	2022-02-20 V5.0.2 
		1：增加Windows服务生成功能。
		2：在工程目录mvc.ini打开SERVICE开关。
		3：管理界面改为中文。
		4：修复MVC.HttpMmt 单元 xe10.3以上版本兼容问题。
		5：添加模板 if 条件的功能。

	2022-02-18 V5.0.1 
		1：新版功能请查看demo源码体会mvc的新特性。