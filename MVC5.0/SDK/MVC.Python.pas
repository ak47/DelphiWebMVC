{*******************************************************}
{                                                       }
{       苏兴迎                                          }
{       E-Mail:pearroom@yeah.net                        }
{                                                       }
{*******************************************************}

unit MVC.Python;
{$I mvc.inc}

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, mvc.Config,
  System.Classes, Winapi.ShellAPI, mvc.JSON, mvc.Net, system.JSON;

type
  TPython = record
    PackageName: string;
    FuncName: string;
    Params: IJObject;
    function exec: string; overload;
    function exec(sPackageName: string; sFuncName: string; sParams: IJObject): string; overload;
  public

  end;

implementation


{ TPython }

function TPython.exec: string;
var
  json: TJSONObject;
begin
  json := TJSONObject.Create;
  try
    json.AddPair('PackageName', self.PackageName);
    json.AddPair('FuncName', self.FuncName);
    json.AddPair('Params', TJSONObject.ParseJSONValue(self.Params.toJSON));
    Result := IINet.Post(Config.py_url, json.ToJSON);
  finally
    json.Free;
  end;
end;

function TPython.exec(sPackageName: string; sFuncName: string; sParams: IJObject): string;
begin
  Self.PackageName := sPackageName;
  self.FuncName := sFuncName;
  self.Params := sParams;
  Result := self.exec;
end;

initialization
// 启动python服务要执行 publish/python/PythonRun.exe

end.

