{*******************************************************}
{                                                       }
{       苏兴迎                                          }
{       E-Mail:pearroom@yeah.net                        }
{                                                       }
{*******************************************************}

unit MVC.Go;
{$I mvc.inc}

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,mvc.Config,
  System.Classes, Winapi.ShellAPI, mvc.JSON, mvc.Net, system.JSON;

type
  TGo = record
    PackageName: string;
    FuncName: string;
    Params: IJObject;
    function exec: string; overload;
    function exec(sPackageName: string;sFuncName: string; sParams: IJObject): string; overload;
  public

  end;

implementation


{ TPython }

function TGo.exec: string;
var
  json: TJSONObject;
begin
  json := TJSONObject.Create;
  try
    json.AddPair('PackageName', self.PackageName);
    json.AddPair('FuncName', self.FuncName);
    json.AddPair('Params', TJSONObject.ParseJSONValue(self.Params.toJSON));
    Result := IINet.Post(Config.go_url, json.ToJSON);
  finally
    json.Free;
  end;
end;

function TGo.exec(sPackageName: string;sFuncName: string; sParams: IJObject): string;
begin
  Self.PackageName := sPackageName;
  self.FuncName := sFuncName;
  self.Params := sParams;
  Result := self.exec;
end;

initialization
// 启动go服务要执行 publish/go/GoRun.exe
end.

