unit Controller.Index;
{$I mvc.inc}

interface

uses
  System.SysUtils, System.Classes, MVC.Route, MVC.JSON, MVC.Controller,
  MVC.LogUnit, System.JSON, MVC.DataSet, Controller.Base, MVC.Redis, MVC.Python,
  MVC.Go, mvc.DB;

type
  [MURL('', '')]                                      //请求路由为根目录，模板目录为根目录
  TIndexContrller = class(TBaseController)            //首页控制器继承基础控制器
  public
    [MURL('index', TMethod._GET)]                     //index方法的请求访问路径，如果没有设置，访问路径为方法名称 ，默认是get方式访问
    procedure index;

    function verifycode: string;                      //继承父类
    function getdata: string;
    function Intercept: Boolean; override;            //父类拦截器覆盖
  end;

implementation

{ TIndexContrller }

function TIndexContrller.verifycode: string;
begin
  Result := 'data:image/jpeg;base64,' + getvcode;
end;

function TIndexContrller.getdata: string;
begin
  Result := '欢迎使用 DelphiWebMVC';
end;

procedure TIndexContrller.index;
begin
  Show('index');
end;

function TIndexContrller.Intercept: Boolean;
begin
  Result := false; //关闭拦截器
end;

end.

