{*******************************************************}
{                                                       }
{       苏兴迎                                          }
{       E-Mail:pearroom@yeah.net                        }
{                                                       }
{*******************************************************}
{
安装指南：https://my.oschina.net/delphimvc/blog/1581715
我的博客：https://my.oschina.net/delphimvc
技术QQ群: 685072623
开发工具:delphi xe10 以上,xe10.4版本除外
注意:win10系统需管理员权限运行
}
program MVCDemo;
{$I mvc.inc}  {设置编译条件Ctrl+Enter查看文件}

uses
  MVC.App,
  Controller.Base in '..\Controller\Base\Controller.Base.pas',
  Controller.Index in '..\Controller\Controller.Index.pas';

{$R *.res}

begin
  MVCApp.Run();
end.

