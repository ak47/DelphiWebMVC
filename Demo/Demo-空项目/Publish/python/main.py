from socketserver import ThreadingMixIn
from http.server import HTTPServer, BaseHTTPRequestHandler
import json

data = {"code":0,"message":"成功","data":{}}

def doWork(map):
    print(map)       
    obj = __import__(map['PackageName'])
    func=getattr(obj,map['FuncName'])
    data['data']=func(map["Params"])     
    return data


class Resquest(BaseHTTPRequestHandler):
    def do_POST(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        params=self.rfile.readline(int(self.headers['content-length']))        
        sdata=doWork(json.loads(params))       
        self.wfile.write(json.dumps(sdata).encode())
 
class ThTttpServer (ThreadingMixIn, HTTPServer):
     pass
if __name__ == '__main__':
    host = ('localhost', 8888)
    print("MVC-Python Server: %s:%s" % host)    
    server = ThTttpServer(host, Resquest)
    server.serve_forever()
    server.server_close()