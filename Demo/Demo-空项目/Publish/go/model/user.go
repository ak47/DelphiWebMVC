package model

import "mvc/cmd"

//函数和结构体名称首字母必须大写 才能被外部调用 相当于 public类型
type User struct{}

func (p User) Getdata(params interface{}) string {
	var m = params.(map[string]interface{})

	var name = m["name"].(string)
	//经过处理
	var data = make(map[string]interface{})
	data["name"] = "go处理后的数据" + name
	var json = cmd.MapToJson(0, "成功", data)
	return json
}

