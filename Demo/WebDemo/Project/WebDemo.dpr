{*******************************************************}
{                                                       }
{       苏兴迎                                          }
{       E-Mail:pearroom@yeah.net                        }
{                                                       }
{*******************************************************}
{
安装指南：https://my.oschina.net/delphimvc/blog/1581715
我的博客：https://my.oschina.net/delphimvc
我的视频：https://space.bilibili.com/18184783
技术QQ群: 685072623
开发工具:delphi xe10 以上,xe10.4版本除外
注意:win10系统需管理员权限运行
}
program WebDemo;
{$I mvc.inc}  { 设置编译条件Ctrl+Enter查看文件 }

uses
  MVC.App,
  Controller.Base in '..\Controller\Base\Controller.Base.pas',
  Controller.Index in '..\Controller\Controller.Index.pas',
  Controller.Main in '..\Controller\Controller.Main.pas',
  Controller.User in '..\Controller\Controller.User.pas',
  Controller.Jwt in '..\Controller\Controller.Jwt.pas',
  Controller.UpImage in '..\Controller\Controller.UpImage.pas',
  Controller.Echarts in '..\Controller\Controller.Echarts.pas',
  Controller.VIP in '..\Controller\Controller.VIP.pas',
  Controller.Role in '..\Controller\Controller.Role.pas',
  Controller.Pay in '..\Controller\Controller.Pay.pas',
  Controller.Test in '..\Controller\Controller.Test.pas',
  SQLMap in '..\Controller\Base\SQLMap.pas',
  TableMap in '..\Controller\Base\TableMap.pas',
  Model.Index in '..\Model\Model.Index.pas',
  Model.Main in '..\Model\Model.Main.pas',
  Model.User in '..\Model\Model.User.pas';

{$R *.res}

begin
  MVCApp.Run();
end.

