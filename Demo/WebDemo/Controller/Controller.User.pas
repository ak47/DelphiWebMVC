unit Controller.User;

interface

uses
  System.SysUtils, System.Classes, MVC.DataSet, MVC.JSON, Controller.Base,
  mvc.DB, model.User;

type
  [MURL('user', 'user')]
  TUserController = class(TBaseController)
  public
    procedure index;
    procedure add;
    procedure edit;
    procedure print;

    function del(id: string): TResData;
    [MDoc(TSave)]
    function save(map: IJObject): TResData;
    function getrole: string;
    function getData(map: IJObject): string;
    [MURL(TMethod._STOP)]     //禁止外部调用
    function getAllData(map: IJObject): IDataSet;
  end;

implementation

uses
  Controller.Role, TableMap;

{ TMainController }

procedure TUserController.add;
begin
  var role := TRoleController.Create(self);
  SetAttr('role', role.getdata);
  role.Free;
end;

function TUserController.del(id: string): TResData;
var
  conn: IConn;
begin
  conn := IIConn;
  if conn.DB.DelByKey(Tb_Uses, 'id', id) then
  begin
    Result.code := 0;
    Result.message := '删除成功';
  end
  else
  begin
    Result.code := 1;
    Result.message := '删除失败';
  end;
end;

procedure TUserController.edit;
begin
  var role := TRoleController.Create(self);
  SetAttr('role', role.getdata);
  role.Free;
end;

function TUserController.getAllData(map: IJObject): IDataSet;
var
  sql: ISQL;
  roleid: string;
  conn: IConn;
begin
  conn := IIConn;
  roleid := map.GetS('roleid');
  sql := IISQL(Tb_Uses);
  if roleid <> '0' then
    sql.AndEq('roleid', roleid);
  Result := conn.DB.use('db1').Find(sql);
end;

function TUserController.getData(map: IJObject): string;
var
  sql: ISQL;
  page, limit: integer;
  roleid: string;
  conn: IConn;
begin
  conn := IIConn;
  page := map.GetI('page');
  limit := map.GetI('limit');
  roleid := map.GetS('roleid');
  sql := IISQL();
  sql.From('users a,dict_role b');
  sql.Select('a.*,b.rolename');
  sql.AndEq('a.roleid', 'b.id'); //and a.roleid=b.id
  if roleid <> '0' then
    sql.AndEqF('a.roleid', roleid);
  var ds := conn.DB.Find(sql, page - 1, limit);
  Result := GetPage(ds);
end;

function TUserController.getrole: string;
begin
  var role := TRoleController.Create(self);
  Result := role.getdata;
  role.Free;
end;

procedure TUserController.index;
begin
  SetAttr('role', getrole);
  show('index');
end;

procedure TUserController.print;
var
  nowdate: string;
begin
  var user := TUserController.Create(self);
  SetAttr('list', user.getAllData(InputToJSON));
  nowdate := FormatDateTime('yyyy年MM月dd日', Now);
  SetAttr('nowdate', nowdate);
  user.Free;
end;

function TUserController.save(map: IJObject): TResData;
var
  ret: IDataSet;
  id: string;
  sql: ISQL;
  conn: IConn;
  re: boolean;
begin
  var item := TSaveDoc.Create(map);
  var s := item.toJSONObject.toJSON;
  item.Free;   //这里必须释放
  conn := IIConn;
  id := map.GetS('id');
  sql := IISQL;
  conn.DB.StartTransaction; // 事务启动
  try
    try
      if id = '' then
      begin
        // map.Delete('id');
        sql.From(Tb_Uses);
        sql.AndEqF('username', map.GetS('username'));

        ret := conn.DB.Find(sql);
        if ret.IsEmpty then
        begin
          with conn.DB.Add(Tb_Uses) do
          begin
            FieldByName('username').AsString := map.GetS('username');
            FieldByName('realname').AsString := map.GetS('realname');
            FieldByName('roleid').AsString := map.GetS('roleid');
            FieldByName('pwd').AsString := map.GetS('pwd');
            FieldByName('uptime').AsDateTime := Now;
            Post;
            re := true;
          end;
        end
        else
        begin
          re := false;
        end;
      end
      else
      begin
        sql.Clear;
        sql.AndNe('id', id);
        sql.AndEqF('username', map.GetS('username'));
        sql.From(Tb_Uses);
        ret := conn.DB.Find(sql);
        if ret.IsEmpty then
        begin
          with conn.DB.Edit(Tb_Uses, 'id', id) do
          begin
            FieldByName('username').AsString := map.GetS('username');
            FieldByName('realname').AsString := map.GetS('realname');
            FieldByName('roleid').AsString := map.GetS('roleid');
            FieldByName('uptime').AsDateTime := Now;
            Post;
            re := true;
          end;
        end
        else
        begin
          re := false;
        end;
      end;
      conn.DB.Commit; // 事务执行
    except
      conn.DB.Rollback; // 事务回滚
      re := false;
    end;
  finally
    if re then
    begin
      Result.code := 0;
      Result.message := '保存成功';
    end
    else
    begin
      Result.code := -1;
      Result.message := '保存失败';
    end;
  end;
end;

end.

