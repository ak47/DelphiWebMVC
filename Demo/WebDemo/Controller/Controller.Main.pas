unit Controller.Main;

interface

uses
  System.SysUtils, System.Classes, MVC.DataSet, Controller.Base, mvc.DB,
  model.Index;

type
  [MURL('main', 'main')]
  TMainController = class(TBaseController)
  public
    [MURL('index', TMethod._GET)]
    procedure index;
    function menu: string;
  end;

implementation

uses
  TableMap;


{ TMainController }

procedure TMainController.index;
var
  ds: string;
begin
  SetAttr('realname', Session.getValue('username'));
  ds := menu;
  SetAttr('menuls', ds);
  Show('main');
end;

function TMainController.menu: string;
var
  sql: ISQL;
  conn: IConn;
begin
  conn := IIConn;
  sql := IISQL(Tb_dict_menu);
  sql.Order('s_id');
  Result := conn.DB.Find(sql).toJSONArray;
end;

end.

