unit Controller.Role;

interface

uses
  System.SysUtils, System.Classes, MVC.JSON, MVC.DataSet, Controller.Base, mvc.DB;

type
  [MURL('Role', 'Role')]
  TRoleController = class(TBaseController)
  private
  public
    function getdata: string;
    procedure index;
  end;

implementation

uses
  TableMap;

{ TRoleController }

procedure TRoleController.index;
begin
  ShowText('hello');
end;

function TRoleController.getdata: string;
var
  conn: Iconn;
  sql: ISQL;
begin
  conn := IIConn;
  sql := IISQL(tb_dict_role);
  Result := conn.DB.Find(sql).toJSONArray;
end;

end.

