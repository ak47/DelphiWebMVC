unit Controller.Jwt;

interface

uses
  System.SysUtils, System.Classes, Controller.Base, MVC.JWT;

type
  [MURL('jwt', 'jwt')]
  TJwtController = class(TBaseController)
    procedure index;
    procedure getToken;
    procedure checktoken;
  end;

implementation

{ TJwtController }

procedure TJwtController.index;
begin
  Show('index');
end;

procedure TJwtController.checktoken;
var
  token: string;
  ret: string;
  JWT: IJWT;
begin
  JWT := IIJWT;
  token := Request.Authorization; // 获取认证token
  ret := Input('name');
  with JWT.O do
  begin
    if parser('88888888', token) then // 解析token
    begin
      ret := ret + '|' + Id + '|' + Subject + '|' + IssuedAt + '|' + Expiration
        + '|' + claimGet('name') + '|' + claimGet('name2') + '|' + Issuer + '|' + Audience;
      ShowText(ret);
    end
    else
    begin
      ShowText('验证失败');
    end;
  end;
end;

procedure TJwtController.getToken;
var
  s: string;
  JWT: IJWT;
begin
  JWT := IIJWT;
  with JWT.O do
  begin
    Id := GetGUID;
    Subject := '明星';
    claimAdd('name', '周华健');
    claimAdd('name2', '刘德华');
    Expiration := DateTimeToStr(Now + 80);
    IssuedAt := DateTimeToStr(Now);
    sign := '88888888';
    Issuer := 'http://api.test.com';
    Audience := 'http://api.test.com';
    s := compact;
    ShowText(s);
  end;
end;

end.

