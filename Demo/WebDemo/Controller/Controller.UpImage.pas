unit Controller.UpImage;

interface

uses
  System.SysUtils, System.Classes, Controller.Base, MVC.JWT;

type
  [MURL('UpImage', 'UpImage')]
  TUpImageController = class(TBaseController)
    procedure index;
    procedure upimage;
  end;

implementation

{ TUpImageController }

procedure TUpImageController.index;
begin

end;

procedure TUpImageController.upimage;
var
  s: string;
begin
  s := UpFiles();
  Success(0, '�ϴ��ɹ�<br>' + s);
end;

end.

