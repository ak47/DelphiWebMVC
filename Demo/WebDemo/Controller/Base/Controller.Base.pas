unit Controller.Base;

interface

uses
  System.SysUtils, System.Classes, MVC.Route, MVC.JSON, MVC.Controller,
  MVC.DataSet, System.JSON, MVC.TOOL;

type
  MURL = TMURL;
  MDoc = TMDoc;
  TMethod = THTTPMethod;

  TBaseController = class(TController)
  public
    function getVCode: string;                {获取验证码}
    function Intercept: Boolean; override;    {拦截器在控制器的父类添加，每个子类也可以重构，添加自已的拦截器}
    function GetPage(ds: IDataSet): string;
    procedure ShowPage(ds: IDataSet);         {封装分页参数，可根据自己的需要修改}
    constructor Create(AOwner: TController); overload;
  end;

implementation



{ TBaseController }

constructor TBaseController.Create(AOwner: TController);
begin
  self.Request := AOwner.Request;
  self.Response := AOwner.Response;
  self.CreateController;
end;

function TBaseController.GetPage(ds: IDataSet): string;
begin
  Result := '{"code":0' + ', "msg":"获取成功"' + ', "count":' + ds.Count.ToString + ', "data":' + ds.toJSONArray + '}';
end;

procedure TBaseController.ShowPage(ds: IDataSet);
begin
  ShowJSON(GetPage(ds));
end;

function TBaseController.getVCode: string;
var
  code: string;
begin
  Result := IITool.getVCode(code);
  Session.setValue('vcode', code);
end;

function TBaseController.Intercept: Boolean;
begin
//  if Session.getJson('user') = nil then
//  begin
//    Response.SendRedirect('/');    {如果Session中user没有数据返回到登陆界面}
//    Result := True;
//  end
//  else
  Result := False;
end;

end.

