unit Controller.Test;

interface
uses
  System.SysUtils, System.Classes, MVC.JSON, MVC.DataSet, Controller.Base;

type
  [MURL('test', 'test')]
  TtestController = class(TBaseController)
  public
    procedure Index(map:IJObject);
    function test:IJObject;
  end;
implementation

{ TtestController }

procedure TtestController.Index;
begin

end;

function TtestController.test: IJObject;
begin
  var json:=iijobject();
  json.S['ok']:='hllo';
  Result:=json;
end;

end.
