unit Controller.Index;

interface

uses
  System.SysUtils, System.Classes, MVC.Route, MVC.JSON, MVC.Controller, mvc.JWT,
  MVC.LogUnit, System.JSON, MVC.DataSet, Controller.Base, MVC.Redis, MVC.Python,
  MVC.Go, mvc.DB, mvc.Verify, model.Index;

type
  [MURL('')]// 设置空值为根目录
  TIndexContrller = class(TBaseController) // 继承父类
  private
    function getJwt: string;
  public
    [MURL('index', THTTPMethod._GET)]  // index方法的请求访问路径，如果没有设置，访问路径为方法名称
    procedure index;
    procedure login;
    procedure check; // check 没有设置 请求路径，将以check 路径访问
    procedure verifycode;
    procedure socket;
    procedure python;  //python调用案例
    procedure py_showname; //使用python下的 getdata方法
    procedure go;

    function getmenu: IDataSet;
    function getdata: IDataSet;
    [MDoc(TGetone)]
    function getone(map: IJObject): string;

    function Intercept: Boolean; override; // 实现自己的拦截器 ，这里不采取拦截返回 false
  end;

implementation

uses
  SQLMap, TableMap;
{ TIndexContrller }

procedure TIndexContrller.python;
begin
  //python 调用使用的是服务方式，可分离，也可集成。
  var py: TPython;
  var sdata := IIJObject();
  sdata.S['username'] := '测试';
  sdata.S['pwd'] := '测试';
  var s := py.exec('user', 'getuser', sdata); //执行方法
  ShowJSON(s);
end;

procedure TIndexContrller.py_showname;
begin
  //写调用前先开发python代码
  var py: TPython;
  var p := IIJObject();
  p.S['name'] := '赵明明';
  var s := py.exec('project', 'showname', p);
  ShowJSON(s);
end;

function TIndexContrller.getmenu: IDataSet;
begin
  Result := IIConn.DB.Find('select * from users');
end;

procedure TIndexContrller.verifycode;
begin
  ShowText('data:image/jpeg;base64,' + getVCode);
end;

procedure TIndexContrller.check;
var
  vcode, scode: string;
  map: IJObject;
  ds: IDataSet;
  name: string;
begin
  vcode := input('vcode');

  scode := Session.getValue('vcode');

  map := InputToJSON;
  var s := map.toJSON;
  var conn := IIConn;
  ds := conn.DB.Find(Tb_Uses, map);

  if ds.IsEmpty then
  begin
    Fail(-1, '账号密码错误');
  end
  else if vcode.ToUpper = scode.ToUpper then
  begin
    name := ds.ds.FieldByName('realname').Value;
    Session.setValue('username', name);

    Success();
  end
  else
    Fail(-1, '验证码错误');
end;

function TIndexContrller.getdata: IDataSet;
var
  sql: ISQLTpl;
  map: IJObject;
begin
  map := IIJObject();
  map.S['name'] := '管理';
  sql := IISQLTpl('sql\user.xml', 'getall', map);
  Result := IIConn.DB.use('db2').Find(sql);
end;

function TIndexContrller.getJwt: string;
var
  s: string;
  JWT: IJWT;
begin
  JWT := IIJWT;
  with JWT.O do
  begin
    Id := GetGUID;
    Subject := '明星';
    claimAdd('name', '周华健');
    claimAdd('name2', '刘德华');
    Expiration := DateTimeToStr(Now + 80);
    IssuedAt := DateTimeToStr(Now);
    sign := '88888888';
    Issuer := 'http://api.test.com';
    Audience := 'http://api.test.com';
    s := compact;
    Result := s;
  end;
end;

function TIndexContrller.getone(map: IJObject): string;
var
  map1: IJObject;
  sql: ISQLTpl;
  ds: IDataSet;
  conn: IConn;
begin
  var item := TGetone.Create(map);
  var json := item.toJSONObject.toJSON;
  conn := IIConn;
  map1 := IIJObject();
 //ap1.SetS('id', item.id);
  map1.SetS('name', '管理');
  map1.SetS('sex', '男');
  map1.SetS('age', '12');
  // sql := IISQLTpl(sql_users);
  // sql := IISQLTpl('sql\user.xml', 'getone', map);
  // s := sql.AsISQL;
  // sql := IISQLTpl('sql\user.xml', 'saveuser', map);
  // sql := IISQLTpl('sql\user.xml', 'mysql', map);
  // sql := IISQLTpl('sql\user.xml', 'edituser', map);
  // sql := IISQLTpl('sql\user.xml', 'del', map);
  // s := sql.AsISQL;
  // sql := IISQLTpl('sql\user.xml', 'edituser', map);
  // sql.SetKey('saveuser', map);
  // var i: integer := conn.db.use('db2').ExecSQL(sql);
  sql.SetKey('mysql');
  ds := conn.DB.use('db2').Find(sql);
  sql.SetKey('testproc', map1);
  ds := conn.DB.use('db2').Find(sql);
  Result := ds.toJSONArray;
  item.Free;
end;

procedure TIndexContrller.go;
begin
  var go: TGo;
  var json := IIJObject();
  json.S['name'] := '你好';
  var s := go.exec('model', 'getdata', json);
  ShowJSON(s);
end;

procedure TIndexContrller.index;
var
  jo: IJObject;
  Verify: IVerify;
  ret: IJArray;
  msg: string;
  i: integer;
begin
//  msg := getJwt;
//  SetAttr('jwt', msg);
  msg := '';
  SetAttr('username', 'hello');
  SetAttr('kk', '20');
  SetAttr('dd', 'ok');
  jo := IIJObject;
  jo.SetS('name', '你好');
  jo.SetS('sex', '男');
  jo.SetS('idcard', '130124198312');
  jo.SetS('phone', '15512132874');
  SetAttr('data', jo);

  Verify := IIVerify;
  Verify.Add('idcard', VerifyType.vIdCard, '身份证格式错误');
  Verify.Add('phone', VerifyType.vPhone, '手机号错误');

  ret := IIJArray();
  if Verify.Verify(jo, ret) then
    SetAttr('msg', '谢谢支持')
  else
  begin

    for i := 0 to ret.A.Count - 1 do
    begin
      msg := msg + ret.A.Items[i].GetValue<string>('Error') + '|';
    end;
    SetAttr('msg', msg);
  end;
end;

function TIndexContrller.Intercept: Boolean;
begin
  Result := false;
end;

procedure TIndexContrller.login;
begin
  Session.remove('username');
end;

procedure TIndexContrller.socket;
begin
  Show('socket');
end;

end.

