unit Model.User;
 //Controller.User 控制下使用
interface

uses
   mvc.BaseModel;

type
  [TRemarks('保存用户信息')]
  TSave = class(TBaseModel)  //Controller.User 下 Save 方法使用
  public
    [TRemarks('序号')]
    id: string;
    [TRemarks('用户名')]
    username: string;
    [TRemarks('真实姓名')]
    realname: string;
    [TRemarks('角色ID')]
    roleid: string;
    [TRemarks('登录密码')]
    pwd: string;
  end;

implementation

end.

