unit Model.Main;

interface

uses
   mvc.BaseModel;

type
  [TRemarks('获取一条信息')]
  TGetone = class(TBaseModel)
  public
    [TRemarks('序号')]
    id: string;
  end;

implementation

end.

